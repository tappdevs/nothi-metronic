export class MenuConfig {
    public defaults: any = {
        header: {
            self: {},
            'items': [

                {
                    'title': 'ডাক',
                    'root': true,
                    'icon': 'flaticon-add',
                    'toggle': 'click',
                    'alignment': 'left',
                    'page': 'agoto-daak',
                },
                {
                    'title': 'নথি',
                    'root': true,
                    'icon': 'flaticon-add',
                    'custom-class': 'kt-menu__item--active',
                    'toggle': 'click',
                    'alignment': 'left',
                },
                {
                    'title': 'ড্যাশবোর্ড',
                    'root': true,
                    'icon': 'flaticon-add',
                    'toggle': 'click',
                    'alignment': 'left',
                },
                {
                    'title': 'রিপোর্ট',
                    'root': true,
                    'icon': 'flaticon-add',
                    'toggle': 'click',
                    'alignment': 'left',
                },
                {
                    'title': 'অফিস ড্যাশবোর্ড',
                    'root': true,
                    'icon': 'flaticon-add',
                    'toggle': 'click',
                    'alignment': 'left',
                },
                /*{
                    'title': 'Pages',
                    'root': true,
                    'icon-': 'flaticon-add',
                    'toggle': 'click',
                    'custom-class': 'kt-menu__item--active',
                    'alignment': 'left',
                    'translate': 'MENU.PAGES',
                    'submenu': {
                        'type': 'classic',
                        'alignment': 'left',
                        'items': [
                            {
                                'title': 'My Account',
                                'icon': 'flaticon-file',
                                'page': 'index'
                            },
                            {
                                'title': 'Task Manager',
                                'icon': 'flaticon-diagram',
                                'badge': {
                                    'type': 'kt-badge--success',
                                    'value': '2'
                                }
                            },
                            {
                                'title': 'Team Manager',
                                'icon': 'flaticon-business',
                                'submenu': {
                                    'type': 'classic',
                                    'alignment': 'right',
                                    'bullet': 'line',
                                    'items': [
                                        {
                                            'title': 'Add Team Member',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Edit Team Member',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Delete Team Member',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Team Member Reports',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Assign Tasks',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Promote Team Member',
                                            'icon': ''
                                        }
                                    ]
                                }
                            },
                            {
                                'title': 'Projects Manager',
                                'page': '#',
                                'icon': 'flaticon-chat-1',
                                'submenu': {
                                    'type': 'classic',
                                    'alignment': 'right',
                                    'bullet': 'dot',
                                    'items': [
                                        {
                                            'title': 'Latest Projects',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Ongoing Projects',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Urgent Projects',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Completed Projects',
                                            'icon': ''
                                        },
                                        {
                                            'title': 'Dropped Projects',
                                            'icon': ''
                                        }
                                    ]
                                }
                            },
                            {
                                'title': 'Create New Project',
                                'icon': 'flaticon-users'
                            }
                        ]
                    }
                },
                {
                    'title': 'Features',
                    'root': true,
                    'icon-': 'flaticon-line-graph',
                    'toggle': 'click',
                    'alignment': 'left',
                    'translate': 'MENU.FEATURES',
                    'submenu': {
                        'type': 'mega',
                        'width': '1000px',
                        'alignment': 'left',
                        'columns': [
                            {
                                'heading': {
                                    'heading': true,
                                    'title': 'Task Reports',
                                    'bullet': 'dot'
                                },
                                'items': [
                                    {
                                        'title': 'Latest Tasks',
                                        'icon': 'flaticon-map'
                                    },
                                    {
                                        'title': 'Pending Tasks',
                                        'icon': 'flaticon-user'
                                    },
                                    {
                                        'title': 'Urgent Tasks',
                                        'icon': 'flaticon-clipboard'
                                    },
                                    {
                                        'title': 'Completed Tasks',
                                        'icon': 'flaticon-graphic-1'
                                    },
                                    {
                                        'title': 'Failed Tasks',
                                        'icon': 'flaticon-graphic-2'
                                    }
                                ]
                            },
                            {
                                'bullet': 'line',
                                'heading': {
                                    'heading': true,
                                    'title': 'Profit Margins',
                                    'bullet': 'dot'
                                },
                                'items': [
                                    {
                                        'title': 'Overall Profits',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Gross Profits',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Nett Profits',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Year to Date Reports',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Quarterly Profits',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Monthly Profits',
                                        'icon': ''
                                    }
                                ]
                            },
                            {
                                'bullet': 'dot',
                                'heading': {
                                    'heading': true,
                                    'title': 'Staff Management',
                                    'bullet': 'dot'
                                },
                                'items': [
                                    {
                                        'title': 'Top Management',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Project Managers',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Development Staff',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Customer Service',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Sales and Marketing',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Executives',
                                        'icon': ''
                                    }
                                ]
                            },
                            {
                                'heading': {
                                    'heading': true,
                                    'title': 'Tools',
                                    'icon': '',
                                    'bullet': 'dot'
                                },
                                'items': [
                                    {
                                        'title': 'Analytical Reports',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Customer CRM',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Operational Growth',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Social Media Presence',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Files and Directories',
                                        'icon': ''
                                    },
                                    {
                                        'title': 'Audit & Logs',
                                        'icon': ''
                                    }
                                ]
                            }
                        ]
                    }
                },
                {
                    'title': 'Apps',
                    'root': true,
                    'icon-': 'flaticon-paper-plane',
                    'toggle': 'click',
                    'alignment': 'left',
                    'translate': 'MENU.APPS',
                    'submenu': {
                        'type': 'classic',
                        'alignment': 'left',
                        'items': [
                            {
                                'title': 'Reporting',
                                'icon': 'flaticon-business'
                            },
                            {
                                'title': 'Social Presence',
                                'page': 'components/datatable_v1',
                                'icon': 'flaticon-computer',
                                'submenu': {
                                    'type': 'classic',
                                    'alignment': 'right',
                                    'items': [
                                        {
                                            'title': 'Reached Users',
                                            'icon': 'flaticon-users'
                                        },
                                        {
                                            'title': 'SEO Ranking',
                                            'icon': 'flaticon-interface-1'
                                        },
                                        {
                                            'title': 'User Dropout Points',
                                            'icon': 'flaticon-lifebuoy'
                                        },
                                        {
                                            'title': 'Market Segments',
                                            'icon': 'flaticon-graphic-1'
                                        },
                                        {
                                            'title': 'Opportunity Growth',
                                            'icon': 'flaticon-graphic'
                                        }
                                    ]
                                }
                            },
                            {
                                'title': 'Sales & Marketing',
                                'icon': 'flaticon-map'
                            },
                            {
                                'title': 'Campaigns',
                                'icon': 'flaticon-graphic-2',
                                'badge': {
                                    'type': 'kt-badge--success',
                                    'value': '3'
                                }
                            },
                            {
                                'title': 'Deployment Center',
                                'page': '',
                                'icon': 'flaticon-infinity',
                                'submenu': {
                                    'type': 'classic',
                                    'alignment': 'right',
                                    'items': [
                                        {
                                            'title': 'Merge Branch',
                                            'icon': 'flaticon-add',
                                            'badge': {
                                                'type': 'kt-badge--danger',
                                                'value': '3'
                                            }
                                        },
                                        {
                                            'title': 'Version Controls',
                                            'icon': 'flaticon-signs-1'
                                        },
                                        {
                                            'title': 'Database Manager',
                                            'icon': 'flaticon-folder'
                                        },
                                        {
                                            'title': 'System Settings',
                                            'icon': 'flaticon-cogwheel-2'
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }*/
            ]
        },
        aside: {
            self: {},
            items: [
                /*{
                    title: 'Dashboard',
                    root: true,
                    icon: 'flaticon2-architecture-and-city',
                    page: 'panel',
                    translate: 'MENU.DASHBOARD',
                    bullet: 'dot',
                },*/

                {section: 'Components'},
                /*{
                    title: 'Google Material',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon2-browser-2',
                    submenu: [
                        {
                            title: 'Form Controls',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'Auto Complete',
                                    page: 'material/form-controls/autocomplete',
                                    permission: 'accessToECommerceModule'
                                },
                                {
                                    title: 'Checkbox',
                                    page: 'material/form-controls/checkbox'
                                },
                                {
                                    title: 'Radio Button',
                                    page: 'material/form-controls/radiobutton'
                                },
                                {
                                    title: 'Datepicker',
                                    page: 'material/form-controls/datepicker'
                                },
                                {
                                    title: 'Form Field',
                                    page: 'material/form-controls/formfield'
                                },
                                {
                                    title: 'Input',
                                    page: 'material/form-controls/input'
                                },
                                {
                                    title: 'Select',
                                    page: 'material/form-controls/select'
                                },
                                {
                                    title: 'Slider',
                                    page: 'material/form-controls/slider'
                                },
                                {
                                    title: 'Slider Toggle',
                                    page: 'material/form-controls/slidertoggle'
                                }
                            ]
                        },
                        {
                            title: 'Navigation',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'Menu',
                                    page: 'material/navigation/menu'
                                },
                                {
                                    title: 'Sidenav',
                                    page: 'material/navigation/sidenav'
                                },
                                {
                                    title: 'Toolbar',
                                    page: 'material/navigation/toolbar'
                                }
                            ]
                        },
                        {
                            title: 'Layout',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'Card',
                                    page: 'material/layout/card'
                                },
                                {
                                    title: 'Divider',
                                    page: 'material/layout/divider'
                                },
                                {
                                    title: 'Expansion panel',
                                    page: 'material/layout/expansion-panel'
                                },
                                {
                                    title: 'Grid list',
                                    page: 'material/layout/grid-list'
                                },
                                {
                                    title: 'List',
                                    page: 'material/layout/list'
                                },
                                {
                                    title: 'Tabs',
                                    page: 'material/layout/tabs'
                                },
                                {
                                    title: 'Stepper',
                                    page: 'material/layout/stepper'
                                },
                                {
                                    title: 'Default Forms',
                                    page: 'material/layout/default-forms'
                                },
                                {
                                    title: 'Tree',
                                    page: 'material/layout/tree'
                                }
                            ]
                        },
                        {
                            title: 'Buttons & Indicators',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'Button',
                                    page: 'material/buttons-and-indicators/button'
                                },
                                {
                                    title: 'Button toggle',
                                    page: 'material/buttons-and-indicators/button-toggle'
                                },
                                {
                                    title: 'Chips',
                                    page: 'material/buttons-and-indicators/chips'
                                },
                                {
                                    title: 'Icon',
                                    page: 'material/buttons-and-indicators/icon'
                                },
                                {
                                    title: 'Progress bar',
                                    page: 'material/buttons-and-indicators/progress-bar'
                                },
                                {
                                    title: 'Progress spinner',
                                    page: 'material/buttons-and-indicators/progress-spinner'
                                },
                                {
                                    title: 'Ripples',
                                    page: 'material/buttons-and-indicators/ripples'
                                }
                            ]
                        },
                        {
                            title: 'Popups & Modals',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'Bottom sheet',
                                    page: 'material/popups-and-modals/bottom-sheet'
                                },
                                {
                                    title: 'Dialog',
                                    page: 'material/popups-and-modals/dialog'
                                },
                                {
                                    title: 'Snackbar',
                                    page: 'material/popups-and-modals/snackbar'
                                },
                                {
                                    title: 'Tooltip',
                                    page: 'material/popups-and-modals/tooltip'
                                }
                            ]
                        },
                        {
                            title: 'Data table',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'Paginator',
                                    page: 'material/data-table/paginator'
                                },
                                {
                                    title: 'Sort header',
                                    page: 'material/data-table/sort-header'
                                },
                                {
                                    title: 'Table',
                                    page: 'material/data-table/table'
                                }
                            ]
                        }
                    ]
                },*/
                {
                    title: 'নথি ব্যবস্থাপনা',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon2-browser-2',
                    submenu: [
                        {
                            title: 'নথির ধরন',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'ধরন তৈরী',
                                    bullet: 'dot',
                                    page: '/nothi/nothi-create-type'
                                },
                                {
                                    title: 'ধরনসমূহ',
                                    bullet: 'dot',
                                    page: '/nothi/nothi-type'
                                },
                                {
                                    title: 'নথি তৈরী',
                                    bullet: 'dot',
                                    page: '/nothi/nothi-create'
                                },
                                {
                                    title: 'পত্রজারি গ্রুপ',
                                    bullet: 'dot',
                                    page: '/nothi/potrojari-group'
                                },
                            ]
                        },
                        {
                            title: 'গার্ড ফাইল',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'গার্ড ফাইলের ধরন',
                                    bullet: 'dot',
                                    page: '/nothi/guard-file-type'
                                },
                                {
                                    title: 'গার্ড ফাইলের তালিকা',
                                    bullet: 'dot',
                                    page: '/nothi/guard-file-list'
                                },
                                {
                                    title: 'আপলোড গার্ড ফাইল',
                                    bullet: 'dot',
                                    page: '/nothi/guard-file-upload'
                                },
                            ]
                        },

                    ]
                },
                {
                    title: 'নিবন্ধন বহি',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon2-browser-2',
                    submenu: [
                        {
                            title: 'মাস্টার ফাইল',
                            bullet: 'dot',
                            page: '/nothi/master-files'
                        },
                        {
                            title: 'নথি প্রেরণ নিবন্ধন বহি',
                            bullet: 'dot',
                            page: '/nothi/nothi-send-nibondhon-bohi'
                        },
                        {
                            title: 'নথি গ্রহন নিবন্ধন বহি',
                            bullet: 'dot',
                            page: '/nothi/nothi-receive-nibondhon-bohi'
                        },
                        {
                            title: 'নথি নিবন্ধন বহি',
                            bullet: 'dot',
                            page: '/nothi/nothi-nibondhon-bohi'
                        },
                        {
                            title: 'পত্রজারি নিবন্ধন বহি',
                            bullet: 'dot',
                            page: '/nothi/potrojari-nibondhon-bohi'
                        },
                        {
                            title: 'আর্কাইভড নথিসমূহ',
                            bullet: 'dot',
                            page: '/nothi/archived-nothi'
                        },
                    ]
                },
                {
                    title: 'প্রতিবেদন সমূহ',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon2-browser-2',
                    submenu: [
                        {
                            title: 'শাখাভিত্তিক নথিসমূহের তালিকা',
                            bullet: 'dot',
                            page: '/nothi/section-wise-nothi-list'
                        },
                        {
                            title: 'পত্রজারি পেন্ডিং',
                            bullet: 'dot',
                            page: '/nothi/potrojari-pending'
                        },
                        {
                            title: 'শাখাভিত্তিক নথিসমূহের তালিকা',
                            bullet: 'dot',
                            submenu: [
                                {
                                    title: 'সেটিংস',
                                    bullet: 'dot',
                                    submenu: [
                                        {
                                            title: 'নথি সিদ্ধান্তসমূহ',
                                            bullet: 'dot',
                                            page: '/nothi/nothi-decision'
                                        },
                                        {
                                            title: 'নথিতে অনুমতি প্রদান',
                                            bullet: 'dot',
                                            page: '/nothi/set-permission-to-nothi'
                                        },
                                        {
                                            title: 'নথি হতে অনুমতি প্রত্যাহার',
                                            bullet: 'dot',
                                            page: '/nothi/remove-permission-from-nothi'
                                        },
                                        {
                                            title: 'আর্কাইভের জন্য অপেক্ষমাণ নথিসমূহ',
                                            bullet: 'dot',
                                            page: '/nothi/waiting-for-archive'
                                        },
                                    ]
                                },
                            ]
                        },
                    ]
                },

                {
                    title: 'সার-সংক্ষেপ ট্রাকিং',
                    bullet: 'dot',
                    icon: 'flaticon2-expand',
                    page: '/nothi/summery'
                },


            ]
        },
    };

    public get configs(): any {
        return this.defaults;
    }
}
