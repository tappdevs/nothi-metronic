import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreritoComponent } from './prerito.component';

describe('PreritoComponent', () => {
  let component: PreritoComponent;
  let fixture: ComponentFixture<PreritoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreritoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
