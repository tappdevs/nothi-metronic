import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgotoComponent } from './agoto.component';

describe('AgotoComponent', () => {
  let component: AgotoComponent;
  let fixture: ComponentFixture<AgotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
