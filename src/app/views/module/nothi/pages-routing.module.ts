// Angular
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Components
import {BaseComponent} from './base/base.component';
import {ErrorPageComponent} from './content/error-page/error-page.component';
// Auth
import {AuthGuard} from '../../../core/auth';
import {NgxPermissionsGuard} from 'ngx-permissions';
import { AgotoComponent } from './components/agoto/agoto.component';

const routes: Routes = [
    {
        path: '',
        component: BaseComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'agoto',
                component: AgotoComponent
            },
            {path: '', redirectTo: 'agoto', pathMatch: 'full'},
            {path: 'error/:type', component: ErrorPageComponent},
            {path: '**', redirectTo: 'panel', pathMatch: 'full'}
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {
}
